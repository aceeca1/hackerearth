#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet arev)
(defmacro arev [a] `(loop [p# 0 q# (dec (alength ~a ))] (when (< p# q#)
    (let [ap# (aget ~a p#)] (aset ~a p# (aget ~a q#)) (aset ~a q# ap#))
    (recur (inc p#) (dec q#)))))

(defn bomb ^long [^chars s] (loop [i 0 u 3 ans 0] (if (>= i (alength s)) ans
    (recur (inc i) (if (= \B (aget s i)) 1 (inc u))
        (if (and (= \W (aget s i)) (< u 3) (aset s i \U)) (inc ans) ans)))))

(dotimes [_ (read-long)] (let
    [s (.toCharArray ^String (read-line)) ans (bomb s)]
    (arev s) (println (+ ans (bomb s)))))
