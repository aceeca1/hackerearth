#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int bomb(char* s) {
    int u = 3, ans = 0;
    for (int i = 0; s[i]; ++i) {
        if (u < 3 && s[i] == 'W') { s[i] = 'U'; ++ans; }
        if (s[i] == 'B') u = 1; else ++u;
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[100001];
        scanf("%s", s);
        int ans = bomb(s);
        reverse(s, s + strlen(s));
        printf("%d\n", ans + bomb(s));
    }
    return 0;
}
