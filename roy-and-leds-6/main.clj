#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [[^long t ^long r ^long g ^long b] (read-seq-long)] (loop
    [ans [0 0 0 0 0 0 0 0] c 0 t t rl r gl g bl b] (if (zero? t)
        (apply println (map ans [4 2 1 6 3 5 7 0]))
        (let [ans (assoc ans c (inc ^long (ans c)))
            c (if (== rl 1) (bit-xor c 4) c)
            c (if (== gl 1) (bit-xor c 2) c)
            c (if (== bl 1) (bit-xor c 1) c)]
            (recur ans c (dec t)
                (if (== rl 1) r (dec rl))
                (if (== gl 1) g (dec gl))
                (if (== bl 1) b (dec bl)))))))
