#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn dis-m ^long [^long n ^long k] (Math/abs (- k (quot n 2))))

(dotimes [_ (read-long)] (let [n (read-long)] (first (keep-indexed
    #(when (not= -1 %2) (println (+ (dis-m n %) (dis-m n %2))))
    (for [_ (range n)] (.indexOf ^String (read-line) (int \*)))))))
