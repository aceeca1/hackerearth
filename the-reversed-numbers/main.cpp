#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[7], s2[7];
        scanf("%s%s", s1, s2);
        reverse(s1, s1 + strlen(s1));
        reverse(s2, s2 + strlen(s2));
        sprintf(s1, "%ld", strtol(s1, nullptr, 10) + strtol(s2, nullptr, 10));
        reverse(s1, s1 + strlen(s1));
        printf("%ld\n", strtol(s1, nullptr, 10));
    }
    return 0;
}
