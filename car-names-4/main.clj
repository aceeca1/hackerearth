#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let
    [^String s (read-line) z (.length s) z3 (quot z 3)] (println
    (if (and (== z (* 3 z3)) (apply distinct? nil (for [^long i (range 3)]
        (if (apply = (subs s (* z3 i) (* z3 (inc i)))) (.charAt s (* z3 i))))))
    "OK" "Not OK"))))
