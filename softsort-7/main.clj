#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defmacro m [] 1000000007)
(let [s (vec (for [_ (range (read-long))] (read-long)))
    a (loop [^long n (apply max s) i 1 u 3 ans [1]] (if (> i n) ans
        (let [uu (rem (* u i) (m))] (recur n (inc i) uu (conj ans uu)))))]
    (doseq [i s] (println (rem (+ 3 ^long (a i)) (m)))))
