#include <cstdio>
#include <cstdint>
#include <vector>
using namespace std;

constexpr int m = 1000000007;

int main() {
    int t, u = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > u) u = n[ti];
    }
    vector<int> fac3(u + 1);
    fac3[0] = 3;
    for (int i = 1; i <= u; ++i) fac3[i] = int64_t(fac3[i - 1]) * i % m;
    for (int ni: n) printf("%d\n", (fac3[ni] + 3) % m);
    return 0;
}
