#include <cstdio>
using namespace std;

constexpr int m[10]{6, 2, 5, 5, 4, 5, 6, 3, 7, 6};

int main() {
    char s[101];
    scanf("%s", s);
    int ans = 0;
    for (int i = 0; s[i]; ++i) ans += m[s[i] - '0'];
    printf("%d\n", ans);
    return 0;
}
