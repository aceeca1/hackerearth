#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int i = 0;
        for (; i < n; ++i)
            if (a[a[i] - 1] - 1 != i) break;
        if (i < n) printf("not ");
        printf("inverse\n");
    }
    return 0;
}
