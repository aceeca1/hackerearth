#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(try (doseq [i (.listFiles (clojure.java.io/file "."))
    :let [i (clojure.java.io/file i "main.clj")]
    :when (.exists i)]
    (print "\33[1;31m[Failed]\33[0m  " (str i))
    (read-string (str \( (slurp i) \)))
    (read-string (str \[ (slurp i) \]))
    (println "\r\33[1;36m[Passed]\33[0m"))
    (catch Exception e (println)))
