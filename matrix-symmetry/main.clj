#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let [
    a (vec (for [_ (range (read-long))] (read-line)))
    h (= a (rseq a)) v (every? #(= % (clojure.string/reverse %)) a)]
    (println (if h (if v "BOTH" "HORIZONTAL") (if v "VERTICAL" "NO")))))
