#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<string> a(n);
        for (int i = 0; i < n; ++i) {
            a[i].resize(n + 1);
            scanf("%s", &a[i][0]);
            a[i].pop_back();
        }
        bool h = equal(a.begin(), a.end(), a.rbegin());
        bool v = all_of(a.begin(), a.end(), [](const string& s) {
            return equal(s.begin(), s.end(), s.rbegin());
        });
        printf("%s\n", h ? v ? "BOTH" : "HORIZONTAL" : v ? "VERTICAL" : "NO");
    }
    return 0;
}
