#include <cstdio>
#include <cctype>
using namespace std;

int main() {
    char s[101];
    scanf("%s", s);
    for (int i = 0; s[i]; ++i)
        if (islower(s[i])) s[i] = toupper(s[i]);
        else s[i] = tolower(s[i]);
    printf("%s\n", s);
    return 0;
}
