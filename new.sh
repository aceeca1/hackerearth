#!/bin/bash
mkdir -p $1
# if test ! -e $1/main.cpp
# then
# cat > $1/main.cpp <<EOF
# #include <cstdio>
# using namespace std;
#
# int main() {
#     int t;
#     scanf("%d", &t);
#     while (t--) {
#         //
#     }
#     return 0;
# }
# EOF
# fi
if test ! -e $1/main.clj
then
cat > $1/main.clj <<EOF
#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))
EOF
fi
# kwrite $1/main.clj $1/main.cpp &
kwrite $1/main.clj &
