#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn go ^long [^long x ^long a ^long n] (cond
    (pos? a) (quot (- n 1 x) a) (neg? a) (quot x (- a)) :else Long/MAX_VALUE))

(dotimes [_ (read-long)] (let [[n m] (read-seq-long)]
    (loop [x 0 y 0 i (read-long) ans 0] (if (zero? i) (println ans)
        (let [[^long a ^long b] (read-seq-long) u (min (go x a n) (go y b m))]
            (recur (+ x (* u a)) (+ y (* u b)) (dec i) (+ ans u)))))))
