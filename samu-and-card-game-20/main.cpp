#include <cstdio>
#include <climits>
#include <algorithm>
using namespace std;

int go(int x, int a, int n) {
    if (a > 0) return (n - 1 - x) / a;
    if (a < 0) return x / (-a);
    return INT_MAX;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, k, x = 0, y = 0, ans = 0;
        scanf("%d%d%d", &n, &m, &k);
        while (k--) {
            int a, b;
            scanf("%d%d", &a, &b);
            int u = min(go(x, a, n), go(y, b, m));
            x += u * a;
            y += u * b;
            ans += u;
        }
        printf("%d\n", ans);
    }
    return 0;
}
