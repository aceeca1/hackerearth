#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        bool head = true;
        for (int i = 1; i <= n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", (k + 1) * i);
        }
        printf("\n");
    }
    return 0;
}
