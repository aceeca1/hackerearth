#include <cstdio>
#include <cstring>
#include <functional>
using namespace std;

int k, z;
char s[5001];

template <class Op>
int f(Op&& op) {
    int a[256]{}, q = 0, ans = 0, c = 0;
    for (int p = 0; ; ++p) {
        while (q < z && !op(c, k)) if (!a[int(s[q++])]++) ++c;
        if (!op(c, k)) return ans;
        ans += z - q + 1;
        if (!--a[int(s[p])]) --c;
    }
}

int main() {
    scanf("%d%s", &k, s);
    z = strlen(s);
    printf("%d\n", f(greater_equal<int>()) - f(greater<int>()));
    return 0;
}
