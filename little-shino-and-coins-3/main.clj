#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet aupdate)
(defmacro aupdate [a i & f] `(let [i# ~i] (aset ~a i# (~@f (aget ~a i#)))))

(defn s-at [^String s i] (int (.charAt s i)))

(let [k (read-long) s ^String (read-line)] (println (apply - (for [f? [>= >]]
    (loop [a (int-array 256) p 0 q 0 ans 0 c 0] (cond
        (f? c k) (recur a (inc p) q (+ ans (- (.length s) q -1))
            (if (zero? (aupdate a (s-at s p) dec)) (dec c) c))
        (>= q (.length s)) ans
        :else (recur a p (inc q) ans
            (if (== 1  (aupdate a (s-at s q) inc)) (inc c) c))))))))
