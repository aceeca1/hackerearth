#include <cstdio>
#include <vector>
using namespace std;

vector<int> a;

bool put(int n, int k) {
    if (!n) return false;
    if (!(n & (n - 1))) return true;
    if (k >= (int)a.size()) return false;
    if (put(n & a[k], k + 1)) return true;
    return n != (n & a[k]) && put(n, k + 1);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        printf("%s\n", put(-1, 0) ? "YES" : "NO");
    }
    return 0;
}

