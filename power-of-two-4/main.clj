#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn put [^long n [^long s1 & sn]] (and (not (zero? n))
    (or (zero? (bit-and n (dec n))) (and s1 (or
        (put (bit-and n s1) sn)
        (and (not= n (bit-and n s1)) (recur n sn)))))))

(dotimes [_ (read-long)] (read-line)
    (println (if (put -1 (read-seq-long)) "YES" "NO")))
