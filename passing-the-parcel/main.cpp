#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int n;
    char s[1001];
    scanf("%d%s", &n, s);
    vector<int> a;
    int p = -1, u = 0, i = 0;
    for (; s[i]; ++i) if (s[i] == 'b') {
        a.emplace_back(i - p);
        u += i - p;
        p = i;
    }
    a.emplace_back(i - (u - a[0]));
    u = 0;
    p = (n - 3) % (a.size() - 1) + 1;
    for (int i = 2; i <= n; ++i) {
        u += a[p] % i;
        if (u >= i) u -= i;
        if (!--p && n != i + 1) p = a.size() - 1;
    }
    printf("%d\n", u + 1);
}
