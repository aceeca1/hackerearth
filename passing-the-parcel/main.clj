#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn reform [^String s] (loop [i 0 p -1 ans []] (cond
    (>= i (.length s)) (conj ans (- (.length s) ^long (apply + (next ans))))
    (= \a (.charAt s i)) (recur (inc i) p ans)
    :else (recur (inc i) i (conj ans (- i p))))))

(defn f [n a] (loop [i 2 ans 0 p (inc (rem (- n 3) (dec (count a))))]
    (if (> i n) ans (recur (inc i)
        (let [k (+ ans (rem (a p) i))] (if (>= k i) (- k i) k))
        (cond (< 1 p) (dec p) (== (inc i) n) 0 :else (dec (count a)))))))

(println (inc (f (read-long) (reform (read-line)))))
