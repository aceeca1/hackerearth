#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

// Snippet: bitCount64
int bitCount64(uint64_t n) {
	n = (n & 0x5555555555555555) + ((n >> 1)  & 0x5555555555555555);
	n = (n & 0x3333333333333333) + ((n >> 2)  & 0x3333333333333333);
	n = (n & 0x0f0f0f0f0f0f0f0f) + ((n >> 4)  & 0x0f0f0f0f0f0f0f0f);
	n = (n & 0x00ff00ff00ff00ff) + ((n >> 8)  & 0x00ff00ff00ff00ff);
	n = (n & 0x0000ffff0000ffff) + ((n >> 16) & 0x0000ffff0000ffff);
    return int(n) + (n >> 32);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t p, m;
        scanf("%" SCNd64 "%" SCNd64, &p, &m);
        printf("%d\n", bitCount64(p ^ m));
    }
    return 0;
}
