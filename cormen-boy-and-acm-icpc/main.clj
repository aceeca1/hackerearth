#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [[^long n k] (read-seq-long)] (println (apply max (for [_ (range k)]
    (loop [a (vec (read-seq-long)) i 0 ans Long/MAX_VALUE s 0] (cond
        (>= i n) (- ^long (apply + a) (min ans s))
        (>= i (quot n 2)) (recur a (inc i) (min ans s)
            (- (+ s ^long (a i)) ^long (a (- i (quot n 2)))))
        :else (recur a (inc i) ans (+ s ^long (a i)))))))))
