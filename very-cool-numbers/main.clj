#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn coolness [^long n] (Long/bitCount (bit-and n
    (bit-not (bit-shift-left n 1)) (bit-shift-left n 2))))

(let [n (read-long) ans (int-array n)
    a (group-by second (for [i (range n)] (into [i] (read-seq-long))))]
    (loop [^long m (apply max (keys a)) i 1 k []] (when (<= i m)
        (let [c (coolness i) kk (assoc k c (inc ^long (get k c 0)))]
            (doseq [[no r k] (a i)] (aset ans no ^int (apply + (drop k kk))))
            (recur m (inc i) kk))))
    (dotimes [i n] (println (aget ans i))))
