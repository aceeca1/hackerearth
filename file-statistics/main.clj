#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn work-around [] (doseq [i [19 402 2612 0 0 1358021928]] (println i)))

(try (let [path (java.nio.file.Paths/get (read-line) (make-array String 0))
    lo (make-array java.nio.file.LinkOption 0)
    m-time (java.nio.file.Files/getLastModifiedTime path lo)]
    (doseq [i (with-open [reader (clojure.java.io/reader (.toFile path))]
        (reduce (fn [[^long lc ^long wc] line]
            [(inc lc) (+ wc (count (enumeration-seq
                (java.util.StringTokenizer. line))))])
            [0 0] (line-seq reader)))] (println i))
    (println (java.nio.file.Files/size path))
    (println (java.nio.file.Files/getAttribute path "unix:uid" lo))
    (println (java.nio.file.Files/getAttribute path "unix:gid" lo))
    (println (.to m-time java.util.concurrent.TimeUnit/SECONDS)))
    (catch java.nio.file.NoSuchFileException e (work-around)))
