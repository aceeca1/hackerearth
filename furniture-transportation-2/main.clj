#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [[n ^long l ^long m] (read-seq-long)] (println (apply + (for
    [i (partition-by (partial < l) (read-seq-long))]
    (if (< l ^long (first i)) 0 (max 0 (- (count i) m -1)))))))
