#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    unordered_set<string> ban;
    int n;
    scanf("%d", &n);
    while (n--) {
        char s[101];
        scanf("%s", s);
        ban.emplace(s);
    }
    scanf("%d", &n);
    bool head = true;
    while (n--) {
        char s[101];
        scanf("%s", s);
        if (ban.count(s)) continue;
        if (!head) putchar('.');
        head = false;
        putchar(toupper(s[0]));
    }
    printf("\n");
    return 0;
}
