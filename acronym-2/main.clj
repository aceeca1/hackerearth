#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [ban (set (for [_ (range (read-long))] (read-line)))] (read-line)
    (println (clojure.string/join \.
        (for [i (read-seq) :when (not (ban i))]
            (Character/toUpperCase ^char (first i))))))
