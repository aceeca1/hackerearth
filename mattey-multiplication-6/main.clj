#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet do-each)
(defmacro do-each [s & f] (conj (map #(concat f [%]) s) 'do))

(dotimes [_ (read-long)] (let [[n ^long m] (read-seq-long) s (StringBuilder.)]
    (loop [i 63] (when-not (neg? i)
        (when (odd? (bit-shift-right m i))
            (do-each [\( n "<<" i ") + "] .append s))
        (recur (dec i))))
    (println (.substring s 0 (- (.length s) 3)))))
