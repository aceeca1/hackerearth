#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char n[18];
        int64_t m;
        scanf("%s%" SCNd64, n, &m);
        bool head = true;
        for (int i = 63; i >= 0; --i) if ((m >> i) & 1) {
            if (!head) printf(" + ");
            head = false;
            printf("(%s<<%d)", n, i);
        }
        printf("\n");
    }
    return 0;
}
