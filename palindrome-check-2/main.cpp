#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    char s[101];
    scanf("%s", s);
    int z = strlen(s);
    auto revIt = reverse_iterator<char*>(s + z);
    printf("%s\n", equal(s, s + z, revIt) ? "YES" : "NO");
    return 0;
}
