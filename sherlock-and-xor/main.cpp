#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k = 0;
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            k += ~a & 1;
        }
        printf("%" PRId64 "\n", int64_t(n - k) * k);
    }
    return 0;
}
