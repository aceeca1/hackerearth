#include <cstdio>
using namespace std;

int maxLine(int n) {
    int ans = 0;
    while (n--) {
        int a;
        scanf("%d", &a);
        if (a > ans) ans = a;
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a1 = maxLine(n), a2 = maxLine(n);
        if (a1 < a2) printf("Alice\n");
        else if (a1 > a2) printf("Bob\n");
        else printf("Tie\n");
    }
    return 0;
}
