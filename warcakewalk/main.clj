#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (read-line) (println (let [
    ^long a (apply max (read-seq-long)) ^long b (apply max (read-seq-long))]
    (cond (< a b) "Alice" (> a b) "Bob" :else "Tie"))))
