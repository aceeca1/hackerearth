#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(apply println (reduce (fn [[^long x ^long y] i] (case i
    \R [(inc x) y] \U [x (inc y)] \L [(dec x) y] \D [x (dec y)]))
    [0 0] (read-line)))
