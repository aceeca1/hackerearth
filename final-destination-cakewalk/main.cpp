#include <cstdio>
using namespace std;

int main() {
    char s[100001];
    scanf("%s", s);
    int x = 0, y = 0;
    for (int i = 0; s[i]; ++i) switch (s[i]) {
        case 'R': ++x; break;
        case 'U': ++y; break;
        case 'L': --x; break;
        case 'D': --y; break;
    }
    printf("%d %d\n", x, y);
    return 0;
}
