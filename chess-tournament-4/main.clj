#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn win [a ^long a1 ^long a2] (if (zero? ^long ((a (dec a2)) a1)) a1 a2))

(let [n (read-long) k (bit-shift-left 1 n)
    a (vec (for [_ (range (dec k))] (vec (read-seq-long))))]
    (loop [[a1 a2 & an] (range k) ans []] (cond
        a1 (recur an (conj ans (win a a1 a2)))
        (next ans) (recur ans [])
        :else (println (inc ^long (first ans))))))
