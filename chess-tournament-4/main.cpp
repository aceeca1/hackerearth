#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    vector<vector<int>> a(1 << n);
    vector<int> b(1 << n);
    for (int i = 0; i < 1 << n; ++i) {
        a[i].resize(i);
        for (int j = 0; j < i; ++j) scanf("%d", &a[i][j]);
        b[i] = i;
    }
    while (b.size() > 1) {
        int z = 0;
        for (int i = 0; i < (int)b.size(); i += 2)
            b[z++] = a[b[i + 1]][b[i]] ? b[i + 1] : b[i];
        b.resize(z);
    }
    printf("%d\n", b[0] + 1);
    return 0;
}
