#include <cstdio>
#include <climits>
#include <string>
#include <vector>
#include <utility>
using namespace std;

// Snippet: kmpBuild
void kmpBuild(const char* s, int n, vector<int>& a) {
    a.resize(n + 1);
    int k = a[0] = -1;
    for (int i = 1; i <= n; ++i) {
        while (k >= 0 && s[k] != s[i - 1]) k = a[k];
        a[i] = ++k;
    }
}

// Snippet: kmpReduce
template <class T, class Op>
T&& kmpReduce(const vector<int>& a,
    const char* s1, int n1, const char* s2, int n2, T&& init, Op&& op) {
    int k = 0;
    for (int i = 0; i < n2; ++i) {
        while (k >= 0 && (k >= n1 || s1[k] != s2[i])) k = a[k];
        if (op(init, ++k)) return move(init);
    }
    return move(init);
}

int delta(const string& s1, const string& s2) {
    vector<int> a;
    kmpBuild(s2.c_str(), s2.size(), a);
    int c = kmpReduce(a, s2.c_str(), s2.size(), s1.c_str(), s1.size(), 0,
        [](int& init, int k) {
            return init = k, false;
        });
    return s2.size() - c;
}

vector<string> a;

int put(int u, const string& s) {
    if (a.empty()) return u;
    int ans = INT_MAX;
    for (int i = 0; i < (int)a.size(); ++i) {
        string ai = move(a[i]);
        a[i] = move(a.back());
        a.pop_back();
        int ans1 = put(u + delta(s, ai), ai);
        if (ans1 < ans) ans = ans1;
        if (i < (int)a.size()) a.emplace_back(move(a[i]));
        else a.emplace_back();
        a[i] = move(ai);
    }
    return ans;
}

int main() {
    int n;
    scanf("%d", &n);
    a.resize(n);
    for (int i = 0; i < n; ++i) {
        char s[16];
        scanf("%s", s);
        a[i] = s;
    }
    printf("%d\n", put(0, ""));
    return 0;
}
