#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment clojure 1.4 -- reduced is not supported)

(comment Snippet ifndef)
(defmacro ifndef [n & f] (if (resolve n) nil (conj f 'do)))

(comment Snippet reduced)
(ifndef reduced
    (deftype Reduced [n] clojure.lang.IDeref (deref [this] n))
    (defn reduced [n] (Reduced. n))
    (defn reduced? [n] (instance? Reduced n))
    (ns-unmap (find-ns 'user) 'reduce)
    (defn reduce
        ([f [c1 & cn]] (reduce f c1 cn))
        ([f init coll] (cond (reduced? init) (deref init)
            coll (recur f (f init (first coll)) (next coll)) :else init))))

(comment Snippet kmp)
(defn kmp-build [^String s] (let [a (int-array (inc (.length s)))]
    (loop [k (aset a 0 -1) i 1] (cond (> i (.length s)) a
        (or (neg? k) (= (.charAt s k) (.charAt s (dec i))))
            (recur (aset a i (inc k)) (inc i))
        :else (recur (aget a k) i)))))
(defn kmp-reduce [^ints a ^String s1 ^String s2 init f]
    (loop [k 0 i 0 v init] (cond (>= i (.length s2)) v
        (or (neg? k) (and (< k (.length s1))
            (= (.charAt s1 k) (.charAt s2 i))))
        (let [vv (f v (inc k))]
            (if (reduced? vv) (deref vv) (recur (inc k) (inc i) vv)))
        :else (recur (aget a k) i v))))

(defn delta ^long [s1 ^String s2]
    (- (.length s2) ^long (kmp-reduce (kmp-build s2) s2 s1 0 #(do %2))))

(defn put ^long [^long u s a] (if-not (seq a) u
    (loop [p [] [q1 & qn] a ans Long/MAX_VALUE] (if-not q1 ans
        (recur (conj p q1) qn (min ans
            (put (+ u (delta s q1)) q1 (concat p qn))))))))

(println (put 0 "" (for [_ (range (read-long))] (read-line))))
