#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

// Snippet: getPrime
void getPrime(int m, vector<int>& p) {
    p.resize(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
}


int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d ", &n, &m);
        vector<int> p, v(n);
        getPrime(m, p);
        for (;;) {
            char s[770000];
            fgets(s, 770000, stdin);
            char *p = strtok(s, " \n");
            int z = 0;
            while (p) {
                v[z++] = strtol(p, nullptr, 10);
                p = strtok(nullptr, " \n");
            }
            if (z == n) break;
        }
        for (int i = m, z = 0; i; --i) p[i] = z += p[i] < i;
        sort(v.begin(), v.end(), greater<int>());
        int ans = 1;
        for (int i = 0; i < (int)v.size(); ++i) {
            int c = v[i] > m ? 0 : p[v[i]] - i;
            ans = int64_t(ans) * c % 1000000007;
        }
        printf("%d\n", ans);
    }
    return 0;
}
