#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet get-prime)
(defn get-prime [^long n] (loop [p (int-array (inc n)) u 0 i 2] (if (> i n)
    (do (aset p u (inc n)) p)
    (let [pi (aget p i) ^long v (if (zero? pi) (do (aset p u i) i) pi)]
        (loop [w 2] (and (<= (* i w) n) (aset p (* i w) w) (< w v)
            (recur (aget p w))))
        (recur p (if (zero? pi) i u) (inc i))))))

(dotimes [_ (read-long)] (let [[^long n ^long m] (read-seq-long)
    ^ints p (get-prime m) v (loop []
        (let [v (vec (read-seq-long))] (if (== n (count v)) v (recur))))]
    (loop [i m z 0] (when-not (zero? i) (println i z)
        (recur (dec i) (aset p i (if (< (aget p i) i) (inc z) z)))))
    (println (reduce #(rem (* ^long % ^long %2) 1000000007)
        (map-indexed
            #(if (> ^long %2 m) 0 (- (aget p %2) ^long %))
            (sort > v))))))
