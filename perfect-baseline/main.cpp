#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<string> a(k);
        for (int i = 0; i < n; ++i) {
            string s(k + 1, 0);
            scanf("%s", &s[0]);
            for (int j = 0; j < k; ++j) a[j] += s[j];
        }
        for (int i = 0; i < k; ++i) {
            int mid = (n - 1) >> 1;
            nth_element(a[i].begin(), a[i].begin() + mid, a[i].end());
            putchar(a[i][mid]);
        }
        printf("\n");
    }
    return 0;
}
