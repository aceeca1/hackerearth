#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let [[n k] (read-seq-long)]
    (println (apply str (apply map
        (fn [& a] (let [a (vec a)] (nth (sort a) (quot (dec n) 2))))
        (for [_ (range n)] (read-line)))))))
