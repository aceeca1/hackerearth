#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[100001];
        int a[256]{};
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) ++a[int(s[i])];
        scanf("%s", s);
        int i = 0;
        for (; s[i]; ++i) if (a[int(s[i])]) break;
        printf("%s\n", s[i] ? "Yes" : "No");
    }
    return 0;
}
