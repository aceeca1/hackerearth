#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment clojure 1.4 -- cannot set rettag)

(defn accu-lucky [[^long d ^long m ^long y]] (let
    [y1 (quot y 100) y2 (rem y 100)]
    (+ (* 11 y1) (min 11 (max 0 (- y2 3)))
        (if (and (<= 3 y2 13) (or (< (dec y2) m)
            (and (== (dec y2) m) (<= (dec m) d)))) 1 0))))

(defn dec-date [[^long d ^long m ^long y]] (cond
    (> d 1) [(dec d) m y] (> m 1) [31 (dec m) y] :else [31 12 (dec y)]))

(dotimes [_ (read-long)] (let [[s t] (for [i (read-seq)]
    (map #(Long/parseLong %) (clojure.string/split i #":")))]
    (println (- ^long (accu-lucky t) ^long (accu-lucky (dec-date s))))))

