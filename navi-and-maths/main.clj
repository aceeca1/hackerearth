#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet op-m)
(defn add-m ^long [^long a ^long b ^long m] (rem (+ a b) m))
(defn mul-m ^long [^long a ^long b ^long m] (rem (* a b) m))
(defn pow-m ^long [^long a ^long b ^long m] (loop [a a b b ans 1]
    (if (zero? b) ans (recur (mul-m a a m) (quot b 2)
        (if (even? b) ans (mul-m ans a m))))))
(defn div-p ^long [^long a ^long b ^long p] (rem (* a (pow-m b (- p 2) p)) p))

(defmacro m [] 1000000007)
(defn put [[^long ans ^long prod ^long sum ^long k] [^long s1 & sn]]
    (cond s1 (let [ans (put [ans prod sum k] sn)]
        (recur [ans (mul-m prod s1 (m)) (add-m sum s1 (m)) (inc k)] sn))
    (>= k 2) (max ans (div-p prod sum (m))) :else ans))

(dotimes [i (read-long)] (read-line)
    (println "Case" (str \# (inc i) \:) (put [0 1 0 0] (read-seq-long))))
