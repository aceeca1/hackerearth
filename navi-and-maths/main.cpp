#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

constexpr int m = 1000000007;

vector<int> a;
int ans;

// Snippet: powM
int64_t powM(int64_t a, int64_t b, int64_t m) {
    int64_t ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % m;
        a = a * a % m;
    }
    return ans;
}

// Snippet: divP
int64_t divP(int64_t a, int64_t b, int64_t p) {
    return a * powM(b, p - 2, p) % p;
}

void put(int prod, int sum, int k, int i) {
    if (i >= (int)a.size()) {
        if (k >= 2) {
            int t = divP(prod, sum, m);
            if (t > ans) ans = t;
        }
        return;
    }
    put(prod, sum, k, i + 1);
    put(int64_t(prod) * a[i] % m, (sum + a[i]) % m, k + 1, i + 1);
}

int main() {
    int t;
    scanf("%d", &t);
    for (int ti = 1; ti <= t; ++ti) {
        int n;
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        ans = 0;
        put(1, 0, 0, 0);
        printf("Case #%d: %d\n", ti, ans);
    }
    return 0;
}
