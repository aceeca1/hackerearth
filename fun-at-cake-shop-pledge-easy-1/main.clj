#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(println (loop [n (read-long) i 1] (if (<= n i) "Darshak"
    (let [ni (- n i) i2 (* i i)] (if (<= ni i2) "Chandan"
        (recur (- ni i2) (inc i)))))))
