#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn jhool-swinging? [^java.util.BitSet cbc ^long n] (loop [i 1 w 0]
    (let [i3 (* i i i) k (- n i3)] (or (>= w 2) (and (>= k i3)
        (recur (inc i) (if (.get cbc k) (inc w) w)))))))

(let [a (vec (for [_ (range (read-long))] (read-long)))
    ^long m (apply max a) cbc (java.util.BitSet. (inc m))]
    (loop [i 0] (let [i3 (* i i i)]
        (when (<= i3 m) (.set cbc i3) (recur (inc i)))))
    (doseq [^long n a] (loop [n n] (cond
        (zero? n) (println -1)
        (jhool-swinging? cbc n) (println n)
        :else (recur (dec n))))))
