#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

vector<bool> isCubic;

bool isJhoolSwinging(int n) {
    int w = 0;
    for (int i = 1; ; ++i) {
        if (w >= 2) return true;
        int i3 = i * i * i, k = n - i3;
        if (k < i3) return false;
        w += isCubic[k];
    }
}

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    isCubic.resize(m + 1);
    for (int i = 0; ; ++i) {
        int i3 = i * i * i;
        if (i3 > m) break;
        isCubic[i3] = true;
    }
    for (int ni: n) {
        while (ni && !isJhoolSwinging(ni)) --ni;
        printf("%d\n", ni ? ni : -1);
    }
    return 0;
}
