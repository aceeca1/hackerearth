#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int maxCLen(const char* s, const char* t) {
    int c = 0, ans = 0;
    for (int i = 0; s + i < t; ++i)
        if (s[i] != 'C') c = 0;
        else if (++c > ans) ans = c;
    return ans;
}

int main() {
    int n;
    scanf("%d", &n);
    char s[525601], *p = s;
    int ans = 0;
    while (n--) {
        scanf("%s", p);
        int z = strlen(p);
        ans = max(ans, maxCLen(p, p + z));
        p += z;
    }
    printf("%d %d\n", ans, maxCLen(s, p));
    return 0;
}
