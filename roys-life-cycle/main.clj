#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn max-c-len [^String s] (loop [i 0 c 0 ans 0] (if (>= i (.length s)) ans
    (let [cc (case (.charAt s i) \C (inc c) 0)]
        (recur (inc i) cc (max ans cc))))))

(let [s (vec (for [_ (range (read-long))] (read-line)))] (println
    (apply max (map max-c-len s))
    (max-c-len (apply str s))))
