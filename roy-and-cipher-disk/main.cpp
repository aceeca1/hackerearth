#include <cstdio>
using namespace std;

int rotate(char s, char t) {
    return (t - s + 38) % 26 - 12;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[101];
        scanf("%s", s);
        printf("%d", rotate('a', s[0]));
        for (int i = 1; s[i]; ++i)
            printf(" %d", rotate(s[i - 1], s[i]));
        printf("\n");
    }
    return 0;
}
