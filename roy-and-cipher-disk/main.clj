#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn rotate [^long from ^long to] (- (rem (- to from -38) 26) 12))

(dotimes [_ (read-long)] (loop [s ^String (read-line) i 0 ans []]
    (if (>= i (.length s)) (apply println ans)
        (recur s (inc i) (conj ans (rotate
            (int (int (case i 0 \a (.charAt s (dec i)))) (.charAt s i))))))))
