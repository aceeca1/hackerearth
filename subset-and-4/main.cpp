#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int z, n;
        scanf("%d%d", &z, &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            z &= a;
        }
        printf("%s\n", z ? "No" : "Yes");
    }
    return 0;
}
