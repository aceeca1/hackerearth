#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (read-line) (println (loop
    [ans 0 [^long a1 & an] (sort (vec (read-seq-long)))]
    (cond an (recur (inc ans)
        (if (== (inc a1) ^long (first an)) (next an) an))
    a1 (inc ans) :else ans))))
