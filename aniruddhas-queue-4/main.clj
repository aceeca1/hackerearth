#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment clojure 1.4 -- reduced is not supported)

(comment Snippet ifndef)
(defmacro ifndef [n & f] (if (resolve n) nil (conj f 'do)))

(comment Snippet reduced)
(ifndef reduced
    (deftype Reduced [n] clojure.lang.IDeref (deref [this] n))
    (defn reduced [n] (Reduced. n))
    (defn reduced? [n] (instance? Reduced n))
    (ns-unmap (find-ns 'user) 'reduce)
    (defn reduce
        ([f [c1 & cn]] (reduce f c1 cn))
        ([f init coll] (cond (reduced? init) (deref init)
            coll (recur f (f init (first coll)) (next coll)) :else init))))

(dotimes [_ (read-long)] (read-line) (let [a (read-seq-long) s (apply + a)]
    (println (inc ^long (reduce
        (fn [[^long s ^long i] ^long ai] (let [ss (- s ai)]
            (if (pos? ss) [ss (inc i)] (reduced i))))
        [(inc (rem (+ (read-long) s -1) ^long s)) 0] a)))))
