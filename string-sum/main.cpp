#include <cstdio>
using namespace std;

int main() {
    char s[1001];
    scanf("%s", s);
    int ans = 0;
    for (int i = 0; s[i]; ++i) ans += s[i] - 'a' + 1;
    printf("%d\n", ans);
    return 0;
}
