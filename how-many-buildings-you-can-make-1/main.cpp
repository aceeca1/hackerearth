#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        for (int i = 1; i * i <= n; ++i)
            ans += n / i - i + 1;
        printf("%d\n", ans);
    }
    return 0;
}
