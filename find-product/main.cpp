#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int n, ans = 1;
    scanf("%d", &n);
    while (n--) {
        int a;
        scanf("%d", &a);
        ans = int64_t(ans) * a % 1000000007;
    }
    printf("%d\n", ans);
    return 0;
}
