#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet count-if)
(defmacro count-if [coll & f] (let [i# (gensym)] `(long (reduce
    (fn [^long s# ~ (with-meta i# (meta coll))]
        (if (~@f ~i# ) (inc s#) s#)) 0 ~coll ))))

(dotimes [_ (read-long)] (let [^String s (read-line) vowel (set "aeiou")]
    (println (str (- (.length s) 3 (count-if s vowel)) \/ (.length s)))))
