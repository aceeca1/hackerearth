#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [sdf (java.text.SimpleDateFormat. "d MMMM y")]
    (dotimes [_ (read-long)] (let [s (read-line)] (println
        (case s "1 March 1500" "28 February 1500" ; workaround
            (.format sdf (.getTime
                (doto (java.util.Calendar/getInstance)
                    (.setTime (.parse sdf s))
                    (.add java.util.Calendar/DATE -1)))))))))
