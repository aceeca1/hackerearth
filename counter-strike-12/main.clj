#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn pos-1 ^long [^long n] (inc (bit-shift-right (dec n) 63)))

(defn search [#^"[Ljava.lang.Object;" p [^long x ^long y] ^long d ^long ub]
    (let [ub1 (bit-shift-right ub 1)] (and
        (>= d ^long (apply + (for [^long i [x y]]
            (cond (neg? i) (- i) (< i ub) 0 :else (- i (dec ub))))))
        (or (== ub 1)
            (and (aget p 0) (search (aget p 0) [x         y]         d ub1))
            (and (aget p 1) (search (aget p 1) [x         (- y ub1)] d ub1))
            (and (aget p 3) (search (aget p 3) [(- x ub1) (- y ub1)] d ub1))
            (and (aget p 2) (search (aget p 2) [(- x ub1) y]     d ub1))))))

(dotimes [_ (read-long)] (let [[n ^long m d] (read-seq-long)
    root (object-array 4) a (vec (for [_ (range n)] (vec (read-seq-long))))
    ^long lb (apply min (for [[^long x ^long y] a] (min x y)))
    ^long ub (apply max (for [[^long x ^long y] a] (max x y)))
    ub (bit-shift-left (Long/highestOneBit (- ub lb)) 1)]
    (doseq [[^long x ^long y] a] (loop
        [x (- x lb) y (- y lb) p root i (bit-shift-right ub 1)]
        (when-not (zero? i) (let
            [ex (pos-1 (bit-and x i)) ey (pos-1 (bit-and y i)) e (+ ex ex ey)]
            (when-not (aget p e) (aset p e (object-array 4)))
            (recur x y (aget p e) (bit-shift-right i 1))))))
    (let [m1 (bit-shift-right m 1)] (loop [i 0 ans 0] (if (>= i m)
        (println (if (> ans m1) "YES" "NO"))
        (let [[^long x ^long y] (read-seq-long)] (recur (inc i) (if (and
            (< (- m1 (- m i)) ans (inc m1))
            (search root [(- x lb) (- y lb)] d ub))
        (inc ans) ans))))))))
