#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[100001];
        scanf("%s", s);
        int z = strlen(s), q;
        scanf("%d", &q);
        while (q--) {
            int a[256]{}, c = 0;
            char qstr[201];
            scanf("%s", qstr);
            for (int i = 0; qstr[i]; ++i) {
                a[int(qstr[i])] = -1;
                ++c;
            }
            int p = 0, q = 0;
            int64_t ans = 0;
            for (;;)
                if (!c) {
                    ans += z - q + 1;
                    c += !a[int(s[p++])]--;
                } else if (q >= z) break;
                else c -= !++a[int(s[q++])];
            printf("%" PRId64 "\n", ans);
        }
    }
    return 0;
}
