#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet aupdate)
(defmacro aupdate [a i & f] `(let [i# ~i] (aset ~a i# (~@f (aget ~a i#)))))

(defn s-at [^String s i] (int (.charAt s i)))

(dotimes [_ (read-long)] (let [^String s (read-line)] (dotimes [_ (read-long)]
    (let [a (int-array 256) c (count (for [i (read-line)] (aset a i -1)))]
        (loop [p 0 q 0 ans 0 c c] (cond
            (zero? c) (recur (inc p) q (+ ans (- (.length s) q -1))
                (if (== -1 (aupdate a (s-at s p) dec)) (inc c) c))
            (>= q (.length s)) (println ans)
            :else (recur p (inc q) ans
                (if (zero? (aupdate a (s-at s q) inc)) (dec c) c))))))))
