#include <cstdio>
#include <vector>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, s;
        scanf("%d", &n);
        unordered_set<int> u;
        u.emplace(0);
        while (n--) {
            int a;
            scanf("%d", &a);
            vector<int> v;
            v.reserve(u.size());
            for (int i: u) v.emplace_back(i + a);
            u.insert(v.begin(), v.end());
        }
        scanf("%d", &s);
        printf("%s\n", u.count(s) ? "YES" : "NO");
    }
    return 0;
}
