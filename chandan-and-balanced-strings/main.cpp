#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[100001];
        scanf("%s", s);
        int k = 0;
        int64_t ans = 0;
        unordered_map<int, int> p;
        p.emplace(0, 0);
        for (int i = 0; s[i]; ++i) {
            k ^= 1 << (s[i] - 'a');
            auto ret = p.emplace(k, 0);
            if (!ret.second) ans += ++ret.first->second;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
