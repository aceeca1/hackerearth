#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn at ^long [^String s i]
    (bit-shift-left 1 (- (int (.charAt s i)) (int \a))))

(dotimes [_ (read-long)] (loop [^String s (read-line) i 0 k 0 ans 0 p {0 0}]
    (if (>= i (.length s)) (println ans)
        (let [kk (bit-xor k (at s i)) u (inc ^long (p kk -1))]
            (recur s (inc i) kk (+ ans u) (assoc p kk u))))))
