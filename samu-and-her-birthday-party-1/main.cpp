#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <climits>
#include <vector>
#include <algorithm>
using namespace std;

// Snippet: bitCount
int bitCount(int n) {
	n = (n & 0x55555555) + ((n >> 1) & 0x55555555);
	n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
	n = (n & 0x0f0f0f0f) + ((n >> 4) & 0x0f0f0f0f);
	n = (n & 0x00ff00ff) + ((n >> 8) & 0x00ff00ff);
	return (n & 0xffff) + (n >> 16);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) {
            char s[k + 1];
            scanf("%s", s);
            a[i] = strtol(s, nullptr, 2);
        }
        int ans = INT_MAX;
        for (int i = 0; i < 1 << k; ++i)
            if (all_of(a.begin(), a.end(), [&](int ai) { return i & ai; })) {
                int c = bitCount(i);
                if (c < ans) ans = c;
            }
        printf("%d\n", ans);
    }
    return 0;
}
