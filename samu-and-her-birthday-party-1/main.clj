#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (println (let [[n ^long k] (read-seq-long)
    a (vec (for [_ (range n)] (Long/parseLong (read-line) 2)))]
    (apply min (for [^long i (range (bit-shift-left 1 k))
        :when (every? #(not (zero? (bit-and i ^long %))) a)]
        (Long/bitCount i))))))
