#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n == 1) {
            scanf("%d", &n);
            printf("%d\n", n);
        } else {
            scanf(" %*[^\n]");
            printf("0\n");
        }
    }
    return 0;
}
