#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <algorithm>
using namespace std;

// Snippet: bitCount64
int bitCount64(uint64_t n) {
	n = (n & 0x5555555555555555) + ((n >> 1)  & 0x5555555555555555);
	n = (n & 0x3333333333333333) + ((n >> 2)  & 0x3333333333333333);
	n = (n & 0x0f0f0f0f0f0f0f0f) + ((n >> 4)  & 0x0f0f0f0f0f0f0f0f);
	n = (n & 0x00ff00ff00ff00ff) + ((n >> 8)  & 0x00ff00ff00ff00ff);
	n = (n & 0x0000ffff0000ffff) + ((n >> 16) & 0x0000ffff0000ffff);
    return int(n) + (n >> 32);
}

struct WithCount {
    int64_t v;
    int c;
    bool operator<(const WithCount& that) const { return c < that.c; }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<WithCount> a(n);
        for (int i = 0; i < n; ++i) {
            scanf("%" SCNd64, &a[i].v);
            a[i].c = bitCount64(a[i].v);
        }
        stable_sort(a.begin(), a.end());
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%" PRId64, a[i].v);
        }
        printf("\n");
    }
    return 0;
}
