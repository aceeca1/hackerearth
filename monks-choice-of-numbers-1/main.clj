#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let [[n k] (read-seq-long)] (println
    (apply + (take k (sort > (map #(Long/bitCount %) (read-seq-long))))))))
