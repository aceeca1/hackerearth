#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet count-if)
(defmacro count-if [coll & f] (let [i# (gensym)] `(long (reduce
    (fn [^long s# ~ (with-meta i# (meta coll))]
        (if (~@f ~i# ) (inc s#) s#)) 0 ~coll ))))

(defn split-even? [a] (loop [p 0 q (dec (count a)) b 0] (cond
    (> p q)  (zero? b)
    (pos? b) (recur p (dec q) (- b ^long (a q)))
    :else    (recur (inc p) q (+ b ^long (a p))))))

(dotimes [_ (read-long)] (let
    [a (vec (for [_ (range (read-long))] (read-line)))] (println (if (or
        (split-even? (      mapv (fn [  u] (count-if u = \#)) a))
        (split-even? (apply mapv (fn [& u] (count-if u = \#)) a)))
    "YES" "NO"))))
