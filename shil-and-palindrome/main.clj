#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet aupdate)
(defmacro aupdate [a i & f] `(let [i# ~i] (aset ~a i# (~@f (aget ~a i#)))))

(let [a (int-array 26)]
    (doseq [i (read-line)] (aupdate a (- (int i) (int \a)) inc))
    (let [odd (vec (keep-indexed #(when (odd? %2) %) a))]
        (if (> (count odd) 1) (println -1) (let [
            s (apply str (map-indexed (fn [^long i ^long ai]
                (apply str (repeat (quot ai 2) (char (+ i (int \a)))))) a))
            mid (apply str (for [^long i odd] (char (+ i (int \a)))))]
            (println (str s mid (clojure.string/reverse s)))))))
