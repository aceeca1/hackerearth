#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [l (read-long)] (dotimes [_ (read-long)]
    (let [[^long x ^long y] (read-seq-long)] (cond
        (or (< x ^long l) (< y ^long l)) "UPLOAD ANOTHER"
        (== x y) "ACCEPTED" :else "CROP IT"))))
