#include <cstdio>
using namespace std;

int main() {
    int l, t;
    scanf("%d%d", &l, &t);
    while (t--) {
        int x, y;
        scanf("%d%d", &x, &y);
        if (x < l || y < l) printf("UPLOAD ANOTHER\n");
        else if (x == y) printf("ACCEPTED\n");
        else printf("CROP IT\n");
    }
    return 0;
}
