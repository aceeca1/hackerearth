cpp := $(patsubst %/main.cpp, build/%, $(wildcard */main.cpp))

all: $(cpp)

$(cpp): build/%: %/main.cpp
	g++ -std=c++11 -Ofast -Wall -Wextra -Werror -Wfatal-errors -o $@ $<

clean:
	rm -rf build/*

check:
	clojure check.clj

Snippet:
	grep "Snippet" */main.clj > Snippet.txt
	echo >> Snippet.txt
	grep "Snippet" */main.cpp >> Snippet.txt

%/: always
	./new.sh $@

always:
