#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <algorithm>
#include <utility>
using namespace std;

constexpr int u = 1000000;

int64_t gcd(int64_t a1, int64_t a2) {
    while (a2) swap(a1 %= a2, a2);
    return a1;
}

int main() {
    int n, m, o, p;
    scanf("%d%d%d%d", &n, &m, &o, &p);
    int64_t a1 = int64_t(p) * o;
    int64_t a2 = int64_t(n) * o;
    int64_t a3 = int64_t(p) * m;
    int64_t a4 = int64_t(n) * m;
    int64_t a = gcd(gcd(a1, a2), gcd(a3, a4));
    int64_t b = max(max(a1, a2), max(a3, a4));
    printf("%" PRId64, u / (b / a) * u);
    return 0;
}
