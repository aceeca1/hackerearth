#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet gcd)
(defn gcd ^long [^long n1 ^long n2] (if (zero? n2) n1 (recur n2 (rem n1 n2))))
(defn lcm ^long [^long n1 ^long n2] (* (quot n1 (gcd n1 n2)) n2))

(let [u 1000000 [^long n ^long m ^long o ^long p] (read-seq-long)
    a [(* p o) (* n o) (* p m) (* n m)]]
    (println (* u (quot u (quot ^long (apply max a) ^long (reduce gcd a))))))
