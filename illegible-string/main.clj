#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [_ (read-line) s1 (clojure.string/replace (read-line) "w" "vv")
    s2 (clojure.string/replace s1 "vv" "w")]
    (println (.length s2) (.length s1)))

