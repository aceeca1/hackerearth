#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let [n (read-long)
    a (Long/highestOneBit n)        ak (Long/numberOfTrailingZeros a)
    b (Long/highestOneBit (- n a))  bk (Long/numberOfTrailingZeros b)
    m 1000000007 am (rem a m)]
    (println (rem (+
        (* (dec ak) (dec am))
        (if (zero? b) 0 (+ b b -1 (* am (inc bk))))) m))))
