#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cmath>
using namespace std;

constexpr int m = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        int ak = ilogb((long double)n);
        int64_t a = 1LL << ak;
        int bk = ilogb((long double)(n - a));
        int64_t b = 1LL << bk;
        int64_t am = a % m;
        int64_t c1 = (ak - 1) * (am - 1);
        int64_t c2 = a == n ? 0 : b + b - 1 + am * (bk + 1);
        printf("%" PRId64 "\n", (c1 + c2) % m);
    }
    return 0;
}
