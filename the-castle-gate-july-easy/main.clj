#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (println (let [
    n (inc (read-long)) a (Long/highestOneBit n) b (- n a)]
    (quot (+ (* a a) (* b b 3) (* -3 a) (* -3 b) 2) 2))))
