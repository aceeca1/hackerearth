#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a = 1 << ilogb(n + 1), b = n + 1 - a;
        printf("%d\n", (a * (a - 3) + b * (b - 1) * 3 + 2) >> 1);
    }
    return 0;
}
