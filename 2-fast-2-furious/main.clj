#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [_ (read-line) [^long a1 ^long a2]
    (for [_ (range 2)] (let [a (read-seq-long)]
        (apply max (map #(Math/abs (- ^long % ^long %2)) (next a) a))))]
    (cond
        (> a1 a2) (do (println "Dom")   (println a1))
        (> a2 a1) (do (println "Brian") (println a2))
        :else     (do (println "Tie")   (println a1))))
