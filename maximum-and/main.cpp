#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t a, b;
        scanf("%" SCNd64 "%" SCNd64, &a, &b);
        int64_t ans = (b - 1) & b;
        if (a < b - 1) {
            int64_t ans1 = (b - 2) & (b - 1);
            if (ans1 > ans) ans = ans1;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
