#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (let [[^long a ^long b] (read-seq-long)]
    (println (if (== 1 (- b a)) (bit-and a b)
        (max (bit-and b (dec b)) (bit-and (dec b) (- b 2)))))))
