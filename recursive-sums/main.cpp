#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, ans = 8, z = 0;
        scanf("%d", &m);
        while (m--) {
            int64_t len, d;
            scanf("%" SCNd64 "%" SCNd64, &len, &d);
            ans += len * d % 9;
            z += d;
        }
        printf("%d\n", z ? ans % 9 + 1 : 0);
    }
    return 0;
}
