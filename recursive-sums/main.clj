#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(dotimes [_ (read-long)] (loop [m (read-long) ans 8 z 0] (if (zero? m)
    (println (if (zero? z) 0 (inc (rem ans 9))))
    (let [[^long len ^long d] (read-seq-long)]
        (recur (dec m) (+ ans (rem (* len d) 9)) (+ z d))))))
