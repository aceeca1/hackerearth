#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(comment Snippet aupdate)
(defmacro aupdate [a i & f] `(let [i# ~i] (aset ~a i# (~@f (aget ~a i#)))))

(let [a (int-array 10)]
    (doseq [i (read-line)] (aupdate a (- (int i) (int \0)) inc))
    (dotimes [i 10] (println i (aget a i))))
