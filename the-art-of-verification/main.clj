#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(let [k ["username" "pwd" "profile" "role" "key"]
    m (apply hash-map (next (re-find (re-pattern
        (let [re (str "[?&](" (clojure.string/join \| k) ")=(.*)")]
            (str re re re re re))) (read-line))))]
    (doseq [i k] (println (str i \:) (m i))))
