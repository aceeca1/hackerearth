#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    char s[101];
    scanf("%s", s);
    if (strstr(s, "000000") || strstr(s, "111111")) printf("Sorry, sorry!\n");
    else printf("Good luck!");
    return 0;
}
