#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    if (!n) {
        printf("0\n");
        return 0;
    }
    int ans = 0, s = 0;
    for (int i = 0; i < n; ++i) {
        int a;
        scanf("%d", &a);
        if (s > 0) s += a; else s = a;
        if (s > ans) ans = s;
    }
    printf("%d\n", ans);
    return 0;
}
