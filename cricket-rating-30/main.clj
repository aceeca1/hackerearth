#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(if (zero? (read-long)) (println 0) (println (first (reduce
    (fn [[^long ans ^long s] ^long i]
        (let [ss (max i (+ s i))] [(max ans ss) ss]))
    [0 0] (read-seq-long)))))
