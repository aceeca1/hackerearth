#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn read-long ^long [] (Long/parseLong (read-line)))
(defn read-seq [] (enumeration-seq (java.util.StringTokenizer. (read-line))))
(defn read-seq-long [] (map #(Long/parseLong %) (read-seq)))

(defn sub-multiset? [m1 m2]
    (every? (fn [[k ^long v]] (<= v ^long (m2 k 0))) m1))

(dotimes [_ (read-long)] (println (let [
    [m1 m2] (for [i (range 2)] (dissoc (frequencies (read-line)) \ ))
    sub12 (sub-multiset? m1 m2) sub21 (sub-multiset? m2 m1)] (cond
        (= sub12 sub21) "You draw some."
        sub12 "You lose some."
        :else "You win some."))))
